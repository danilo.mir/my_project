#include "dice.h"
#include <stdio.h>

void initializeSeed(){
  srand(time(NULL));
}

int rollDice(){
  int num;
  scanf("%d", &num);
  return (rand() % num)+1;
}
